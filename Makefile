include paths.mk

CC=gcc-10
CFLAGS= -Wl,-rpath,$(LIBPATH) -L$(LIBPATH) -fpic -fopenmp $(COPTFLAGS) $(OPTS)
ARCH=Carmel
ifeq ($(ARCH),Carmel)
	ARCHFLAGS= -mtune=cortex-a76  -march=armv8.2-a+fp16fml -Dfp16_support #NVIDIA Carmel
else ifeq ($(ARCH),A57)
	ARCHFLAGS= -mtune=cortex-a57 -march=armv8-a+fp+simd -mcpu=cortex-a57 #Cortex A-57
endif
COPTFLAGS= -O3 -ftree-vectorize $(ARCHFLAGS) -funsafe-math-optimizations -ffp-contract=fast 
LIB= -lblis -lm  

uKOBJS=  gemm_armv8a_asm_d6x8.o  

.PHONY: all clean test comp

all: testTransPrec.x testIm2Col.x testGemm.x convEval.x
test: testTransPrec.x
comp: testGemm.x
im2col: testIm2Col.x
eval: convEval.x
lib: $(LIBPATH)/libtransPrecision.so


%.o: %.c 
	$(CC) -c -o $@ $< $(CFLAGS) -I$(INCLUDE)
	
transPrecision.o: transPrecision.c $(uKOBJS) transPrecision.h
	$(CC) -c -o $@ $< $(CFLAGS) -I$(INCLUDE)

$(LIBPATH)/libtransPrecision.so: transPrecision.o 
	$(CC) -shared -fopenmp -o $@  $< $(uKOBJS)
	
%.x: %.o $(LIBPATH)/libtransPrecision.so 
	$(CC) -o $@ $^  $(CFLAGS) $(LIB)

clean:
	rm *.o

