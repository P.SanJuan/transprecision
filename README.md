Introduction
-------------
This reposirory contains the currently completed work within the project *Decoupling storage from arithmetic support in DL framework*.  This is a project under development and major changes to this repository are expected to happen during the next months.

Requirements
------------------
Although the transprecision algorithms does not require the BLIS library ([https://github.com/flame/blis](https://github.com/flame/blis)), this library should be available in order to compile and execute the testing applications.

The file paths.mk should be edited to point to the proper LIB and INCLUDE directories that contain the library.

Compilation
------------

This repository contains a library and 2 different  test programs, they can be all compiled with:
`make` or `make all`

Each program has its own target and some compilation options that should be pased to make as:
`make OPTS="Options"`
- `make test`: compiles the transprecision tester. This program performs a conversion to a low-precision format followed by a conversion from that format to IEEE floating-point precision. Then it compares the result with the original value and offers  an approximation error measurement.
- `make comp`:  compiles the gemm comparator. This program compares the gemm implemented in BLIS with the transprecision GEMM (based in BLIS architecture) used in this project. Options:
	 - `-Dout_csv` : Formats the output as a semicolon separated CSV.
- `make conv`: compiles the transprecision convolution tester. This program compares the im2col + gemm approach against our transprecision CONVGEMM algorithm (convolution using a gemm with implicit im2col).

The compilation is optimized for ARM Cortex A-57 and NVIDIA Carmel. 
The library and testers had been tested in a NVIDIA Jetson Nano and NVIDIA Jetson Xavier.

Funding
------------
This project has been founded by the OPRECOMP Summer of Code 2020  http://oprecomp.eu/open-source/ under  the European Union’s H2020-EU.1.2.2. - FET Proactive research and innovation programme under grant agreement #732631.
