/**GEMM comparator
 * 
 * This test performs a comparison between BLIS GEMM and the custom GEMM.
 * The test is performed for ARMCortex  A-57
 * 
 * @author P. San Juan
 * @date 04/2020
 */


#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "transPrecision.h"
#include <blis.h>




double compareMatrix(int m, int n, float *M, int ldm, float *M2, int ldm2 );
int print_matrices( int m, int n, char *name, float *M, int ldm,  char *name2, float *M2, int ldm2);




int main( int argc, char** argv )
{
    double tBlis, tTrans, tIni,
            gflopsBlis, gflopsTrans;
            
    char* precision;
    
    float ONE = 1, 
          ZERO = 0, 
          norm = 0;
    
    int i,
        m, n, k, //matrix dimms
        repe;
    
    float *A, *B, *C, *CTrans,
          *Ac_pack, *Bc_pack;
    bits* A_prec_red;

    

    

    
    if (argc != 5)
    {
        printf("Comparison of blis default GEMM and a transprecision GEMM.\n");
        printf("\tm,n,k: Matrix product dimensions.\n");
        printf("\trepe: number of repetitions of the test.\n");
        printf("Usage: %s <m> <n> <k> <repe> \n", argv[0]);
        return -1;
    }

    m = atoi(argv[1]);  
    n = atoi(argv[2]);
    k = atoi(argv[3]);
    repe = atoi(argv[4]);

    A = (float*) aligned_alloc(ALIGN,m*k * sizeof(float));
    B = (float*) aligned_alloc(ALIGN,k*n * sizeof(float));
    C = (float*) aligned_alloc(ALIGN, m*n * sizeof(float));
    
    A_prec_red = (bits*) aligned_alloc(ALIGN,m*k * sizeof(bits));
    CTrans= (float*) aligned_alloc(ALIGN, m*n * sizeof(float));
    
    //alloc auxiliar matrices
    alloc_pack_buffs( &Ac_pack, &Bc_pack);

    

    bli_srandm( 0, BLIS_DENSE, m, k, A, 1, m );
    bli_srandm( 0, BLIS_DENSE, k, n, B, 1, k );
    
    stransPrecShrink(fp16,m*k,A,A_prec_red);


    //Timing gemm blis
    tIni = bli_clock();
    for(i = 0; i <  repe; i++)
        bli_sgemm(BLIS_NO_TRANSPOSE,BLIS_NO_TRANSPOSE,m,n,k,&ONE,A,1,m,B,1,k,&ZERO,C,1,m);
    tBlis = bli_clock() - tIni;   
    
    //Timing custom gemm 
    tIni = bli_clock();
    for(i = 0; i <  repe; i++)
    {
        sgemm_transPrec(m,n,k,1.0,fp16,A_prec_red,m,B,k,0.0,CTrans,m,Ac_pack,Bc_pack);
    }
    tTrans = bli_clock() -tIni;
  
                

    norm = compareMatrix(m,n,CTrans, m,C,m);
    //print_matrices( m, n,  "Own", COwn, m, "Blis", CBlis, m);
    printf("Approximation error: %g\n",(double)norm);
    
    
    tBlis /=repe;
    tTrans/=repe;
    
    gflopsBlis = ( 2.0 * m * k * n ) / ( tBlis * 1.0e9 );
    gflopsTrans = ( 2.0 * m * k * n ) / ( tTrans * 1.0e9 );
    
    



        
   // printf("BLIS Time: %.3f GFlops: %.3f\n",tBlis,gflopsBlis);
    //printf("Custom Time: %.3f GFlops: %.3f\n",tTrans,gflopsTrans);

#ifdef out_csv
    printf("%d;%d;%d;%.4f;%.3f;%.4f;%.3f\n",m,n,k,tBlis,gflopsBlis,tTrans,gflopsTrans);
#else
    printf("Size[%d,%d,%d],BLIS[T=%.4f,P=%.3f],Custom[T=%.4f,P=%.3f]\n",m,n,k,tBlis,gflopsBlis,tTrans,gflopsTrans);
#endif
    
    free(A);
    free(B);
    free(C);
    free(CTrans);
    free(Ac_pack);
    free(Bc_pack);
    free(A_prec_red);
    
}

double compareMatrix(int m, int n, float *M, int ldm, float *M2, int ldm2 )
{
    int i,j;
    double Aux = 0;
    double Aux2 = 0;
    
    
    for(j=0; j < n; j++)
        for(i=0; i < m; i++)
        {
            Aux += pow(M[i+j*ldm] - M2[i + j* ldm2],2);
            Aux2+= M[i+j*ldm] * M[i+j*ldm];
            
        }
    return sqrt(Aux)/sqrt(Aux2);
}




int print_matrices( int m, int n, char *name, float *M, int ldm,  char *name2, float *M2, int ldm2)
{

  int i, j;

  for ( j=0; j<n; j++ )
    for ( i=0; i<m; i++ )
      printf( "   (%d,%d) = %s[%22.15e]    [%22.15e]%s;\n",  i, j,name, (double)M[i +j * ldm], (double)M2[i +j * ldm2],name2);

  return 0;
}




