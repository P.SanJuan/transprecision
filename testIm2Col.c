/**Convolution tester
 * 
 * This test compares the naive convolution  with the imtocol + gemm and the convGemm approaches.
 * The test is performed for ARMCortex  A-57
 * 
 * @author P. San Juan
 * @date 04/2020
 */


#include "transPrecision.h"

float compareMatrix(const int m,const int n, float* M, const int ldm, float* M2, const int ldm2);
void im2Col(const int h, const int w, const int c, const int b,const float* In, const int kh, const int kw, const int stride,float* Out);

int main( int argc, char** argv )
{
    double tConv = 0.0, tIm2Col = 0.0, tIm2ColGemm = 0, tConvGemm = 0.0, tIni,
            perfPeak,perfGemm, perfConvGemm;
            
    float ONE = 1, ZERO = 0;
    
    int i, m, n, k, 
        repe, //repetitions
        iZERO = 0;
        
     int h,w,c,b, //input dims
         ho,wo, //oputput dimms
         kh,kw, kn, //kernel dimms
         stride,pad; //algorithm parameters
        
    
    float *F, *In, 
         *OutI2c, *OutConvGemm,
         *Aux,
         *Ac_pack, *Bc_pack;
    
    bits *F_fp16;
         
    
    if (argc != 11)
    {
        printf("Comparison of Convolution implementations.\n");
        printf("\th,W,c: Imput tensor dimensions dimensions.\n");
        printf("\tb: Batch size.\n");
        printf("\tkh, kw: Kernel dimensions.\n");
        printf("\tkn: Kernel number.\n");
        printf("\tStride: kernel aplication stride.\n");
        printf("\tpad: Imput 0 padding.\n");
        printf("\trepe: number of repetitions of the test.\n");
        printf("Usage: %s <h> <w> <c> <b> <kh> <kw> <kn> <stride> <pad> <repe>\n", argv[0]);
        return -1;
    }

    h =atoi(argv[1]);  
    w =atoi(argv[2]);
    c = atoi(argv[3]);
    b  =atoi(argv[4]);
    kh  =atoi(argv[5]);
    kw  =atoi(argv[6]);
    kn  =atoi(argv[7]);
    stride =atoi(argv[8]);
    pad =atoi(argv[9]);
    repe =atoi(argv[10]);
                
    printf("Allocating matrices...\n");
    
    //input matrices
    In = (float*) aligned_alloc(ALIGN, h*w*c *b * sizeof(float));
    F = (float*) aligned_alloc(ALIGN, kh*kw*c *kn * sizeof(float));
    F_fp16 = (bits*) aligned_alloc(ALIGN,kh*kw*c *kn * sizeof(bits));
    
    ho = floor((h - kh + 2 * pad) / stride + 1);
    wo = floor((w - kw + 2 * pad) / stride + 1);
    
    //auxiliar matrices
    alloc_pack_buffs( &Ac_pack, &Bc_pack);
    Aux = (float*) aligned_alloc(ALIGN, c*kh*kw * ho*wo*b * sizeof(float));
    
    //output matrices 
    OutI2c= (float*) aligned_alloc(ALIGN, ho*wo*kn*b * sizeof(float));
    OutConvGemm = (float*) aligned_alloc(ALIGN, ho*wo*kn*b * sizeof(float));
    
    
    printf("Generating random matrices...\n");
    bli_srandm( 0, BLIS_DENSE, b, h*w*c, In, 1, b );
    bli_srandm( 0, BLIS_DENSE, kn, kh*kw*c, F, 1, kn );
    
    stransPrecShrink(fp16, kn*kh*kw*c, F, F_fp16);
    
    printf("Starting evaluation...\n");


        //Timing im2col +gemm
    tIni = bli_clock();
    for(i = 0; i <  repe; i++)
    {
        im2Col (h,w,c,b,In,kh,kw, stride,Aux);
        bli_sgemm(BLIS_NO_TRANSPOSE,BLIS_NO_TRANSPOSE,kn,ho*wo*b,kh*kw*c,&ONE,F,1,kn,Aux,1,kh*kw*c,&ZERO,OutI2c,1,kn);
    }  
    tIm2ColGemm += bli_clock() -tIni;
        
   
        
    //Timing implicint gemm
    tIni = bli_clock();
    for(i = 0; i <  repe; i++)
    {
        sconvGemm_transPrec(kh,kw,c,kn,1,fp16,F_fp16, h,w,b, stride, In, 0,OutConvGemm,Ac_pack,Bc_pack);
    }
    tConvGemm += bli_clock() -tIni;

    
    

    tIm2ColGemm/=repe;
    tConvGemm/=repe;
    
    perfConvGemm = ( 2.0 * kn*ho*wo*b*kh*kw*c ) / ( tConvGemm * 1.0e9 );
    perfGemm = ( 2.0 * kn*ho*wo*b*kh*kw*c ) / ( (tIm2ColGemm-tIm2Col) * 1.0e9 );
    perfPeak = 2.035 * 8 ;// cpuFreq * flopscycle
    
    printf("Convolution Time: %.4g \n",tConv);
    printf("im2Col + Gemm Time: %.4g [Im2Col: %.4g , Gemm: %.4g] GFLOPS(gemm): %.5g \n",tIm2ColGemm, tIm2Col, tIm2ColGemm -tIm2Col,perfGemm);
    printf("Implicit Gemm Time: %.4g GFLOPS: %.5g\n",tConvGemm,perfConvGemm);
    printf("Peak performance: %g, im2col gemm: %g, implicit gemm: %g\n",perfPeak,perfGemm/perfPeak,perfConvGemm/perfPeak);
    
    
    printf("norm(OutIm2col-OutConvGemm)=%g\n",compareMatrix(kn,ho*wo*b,OutI2c,kn,OutConvGemm,kn));

    
    free(In);
    free(F);
    free(OutI2c);
    free(OutConvGemm);
    free(Aux);
    free(Ac_pack);
    free(Bc_pack);
}


/** Compares two matrices and returns the difference between them.
 * 
 * @param m Rows of matrices.
 * @param n Cols of matrices.
 * @param M First matrix.
 * @param ldm Leading dimension of M. 
 * @param M2 Second matrix.
 * @param ldm2 Leading dimension of M2.
 * @return  |N-M2|_F/|M2|_F
 * */
float compareMatrix(const int m,const int n, float* M, const int ldm, float* M2, const int ldm2){
    

    float norm = 0, normM2 = 0;
    
    bli_snormfv(m*n,M2,1,&normM2);
    bli_ssubm(0,BLIS_NONUNIT_DIAG,BLIS_DENSE,BLIS_NO_TRANSPOSE,m,n,M,1,ldm,M2,1,ldm2);
    bli_snormfv(m*n,M2,1,&norm);
    
    return norm/normM2;
}


/** Performs a im2col transformation to the input tensor.
 * 
 * Applys the im2Col tranform to the input tensor. The im2col transform uis used to perform a convolution using the GEMM kernel.
 * 
 * @param[in] h input tensor hight
 * @param[in] w input tensor width
 * @param[in] c number of chanels of input tensor
 * @param[in] b batch Size
 * @param[in] In 1D-array containing a flattened version of the input tensor
 * @param[in] kh kernel height
 * @param[in] kw kernel width
 * @param[in] stride Stride to apply the krnels to the input tensor
 * @param[out] Out Matrix (column major stored) containing the expanded matrix
 */
void im2Col(const int h, const int w, const int c, const int b,const float* In, const int kh, const int kw, const int stride,float* Out)
{
    int ic, ikh, ikw, ih, iw, ib,
        row,col, ho,wo,pad =0; //padding currently unsuported
   
    ho = floor((h - kh + 2 * pad) / stride + 1);
    wo = floor((w - kw + 2 * pad) / stride + 1);
   
    unsigned int cSize = h*w, //chanel memory leap in input tensor
                 coSize = ho*wo, //chanel memory leap in output matix
                 kSize = kh*kw, //kernel memory leap (single chanel)
                 bSize = c*h*w, //batch memory leap
                 ckSize = c * kSize, //kernels memory leap (all channels)
                 posib,
                 posic,
                 posiw,
                 posih,
                 posikw,
                 rowic,
                 rowikw,
                 colib,
                 coliw;
    
                 

    
    for(ib = 0;ib < b; ib++)    
    {
        colib = ib * coSize;
        posib = ib * bSize;
        #pragma omp parallel for private (iw,ih,ikw,ikh,row,col,rowic,posic,coliw,posiw,posih,rowikw,posikw) 
        for( ic = 0; ic < c; ic++)
        {
            rowic = ic *kSize;
            posic = ic * cSize + posib;
            for(iw = 0; iw < wo; iw++)   
            {
                coliw = colib + iw * ho;
                posiw = iw * stride * h + posic;
                for(ih = 0; ih < ho; ih++)
                {
                     //OPT col = ib * coSize + iw * ho + ih;
                    col = coliw + ih;
                    posih = stride * ih;
                    for(ikw = 0; ikw < kw; ikw++)
                    {
                        rowikw = rowic + ikw * kh;
                        posikw = posiw + ikw * h;
                        for(ikh = 0; ikh < kh; ikh++)
                        {
                             //OPT row = ic *kSize + ikw * kh + ikh; 
                            row = rowikw + ikh;
                            //printf("Writing into Out[%d,%d] from In[%d,%d,%d,%d]\n",
                              //    row,col,ib,ic, (iw * stride + ikw),(stride * ih + ikh));
                            //OPT Out[row + col * ckSize] = In[ib * bSize + ic * cSize + (iw * stride + ikw) * h + (stride * ih + ikh)];
                            //OPT Out[row + col * ckSize] = In[posib + posic + posikw + posih + ikh];
                            Out[row + col * ckSize] = In[posikw + posih + ikh];

                        }
                    }
                }   
            }
        }
    }
}
