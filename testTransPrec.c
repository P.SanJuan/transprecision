
#include "transPrecision.h"


/** Compares two matrices and returns the difference between them.
 * 
 * @param m Rows of matrices.
 * @param n Cols of matrices.
 * @param M First matrix.
 * @param ldm Leading dimension of M. 
 * @param M2 Second matrix.
 * @param ldm2 Leading dimension of M2.
 * @return  |N-M2|_F/|M2|_F
 * */
float compareMatrix(const int m,const int n, float* M, const int ldm, float* M2, const int ldm2){
    

    float norm = 0, normM2 = 0;
    
    bli_snormfv(m*n,M2,1,&normM2);
    bli_ssubm(0,BLIS_NONUNIT_DIAG,BLIS_DENSE,BLIS_NO_TRANSPOSE,m,n,M,1,ldm,M2,1,ldm2);
    bli_snormfv(m*n,M2,1,&norm);
    
    return norm/normM2;
}


int main( int argc, char** argv )
{

    int n = 100;
    float*  f32 =  (float*) malloc(n * sizeof(float));
    float* expanded = (float*) malloc(n * sizeof(float));
    _Float16*  shrinked = (_Float16*) malloc(n*sizeof(_Float16));
    
    bli_srandm( 0, BLIS_DENSE, n, 1, f32, 1, n );

    stransPrecShrink(fp16,n,f32,shrinked);
    stransPrecExpand(fp16,n,shrinked, expanded);
    
    //printf("float=%X; half=%X; expanded=%X\n",*(unsigned*)&f32, *(unsigned short *)shrinked, *(unsigned *)expanded);
    
     printf("norm(original-expanded)=%g\n",compareMatrix(n,1,f32,n,expanded,n));
    
}
