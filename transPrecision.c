/**GEMM and GEMM_conv code
 * 
 * This file contains the implementation of all functions related with the GEMM computation 
 * for multiple datatypes. Also contains the CONVGEMM algorithm first presented in
 * https://doi.org/10.1109/SBAC-PAD49847.2020.00023, and its variations.
 * The code present in this file has evolved from a code originally developed for the 
 * following paper: https://doi.org/10.1007/s10586-019-02927-z
 * 
 * @author P. San Juan
 * @date 04/2020
 */

#include "transPrecision.h"



/**
 * Allocates the packing buffers Ac_pack and Bc_pack with
 * proper size and alignment.
 *
 * @param[out] Ac_pack Buffer to contain packed portions of A.
 * @param[out] Bc_pack Buffer to contain packed portions of B.
 * @return 0 on success, 1 on allocation error.
 */
int alloc_pack_buffs(float** Ac_pack, float** Bc_pack)
{
    *Ac_pack =  (float *) aligned_alloc(ALIGN, BLOCK_MC * BLOCK_KC * sizeof(float));
    *Bc_pack =  (float *) aligned_alloc(ALIGN, BLOCK_KC * BLOCK_NC * sizeof(float));
    
    if(Ac_pack == NULL || Bc_pack == NULL)
        return 1;
    return 0;
}

/**
 * Single precision xpby for matrices
 *
 * Performs the operation  Y = X + beta * Y. All matrices are expected to be
 * stored in column major order.
 *
 * @param[in] m Number of rows of matrices X and Y.
 * @param[in] n Number of columns of matrices X and Y
 * @param[in] X Matrix to add.
 * @param[in] ldx Leading dimension of matrix X.
 * @param[in] beta Scalar to multiply Y.
 * @param[in,out] Y Input and output matrix.
 * @param[in] ldy Leading dimension of matrix Y.
 */
void sxpbyM(unsigned int m, unsigned int n, const float *restrict X, unsigned int ldx,
            const float *restrict beta, float *restrict Y, unsigned int ldy)
{
    unsigned int i, j;
    for(j = 0; j < n; j++)
        for(i = 0; i < m; i++)
            *(Y + i + j * ldy) =
                *(X + i + j * ldx) + *beta * *(Y + i + j * ldy);
}

/**
 * Sets all the elements of a single precision matrix to 0.
 *
 * @param[in] m Number of rows of matrix M.
 * @param[in] n Number of columns of matrix M.
 * @param[in,out] M Matrix to set.
 * @param[in] ldm Leading dimension of matrix M.
 */
void sset0sM(unsigned int m, unsigned int n, float *restrict M,
             unsigned int ldm)
{
    unsigned int i, j;

    #pragma omp parallel for private(i)
    for(j = 0; j < n; j++)
        for(i = 0; i < m; i++)
            *(M + i + j * ldm) = 0;
}


/** Packing of simple precision matrix A.
 * 
 * Packs a block of matrix A into a buffer A_pack in the proper data arrengment 
 *  that the microkernel needs.
 * 
 * @param[in] A Pointer pointing to the position of A to start packing.
 * @param[in] lda Leading dimension of matrix a.
 * @param[in] A_pack Buffer containing the portion of A packed.
 * @param[in] m Height of the block to pack.
 * @param[in] k Width of the block to pack.
 */
void sPack_A_transPrec(fpType type_desc, bits *A, unsigned int lda, float *A_pack, unsigned int m, unsigned int k)
{
	float *A_pack_local;
    unsigned int skipPos;
    
	#pragma omp  parallel for private(A_pack_local,skipPos)
	for(unsigned int ic=0;ic<m;ic+=BLOCK_MR){

		A_pack_local= &A_pack[ic*k];
		unsigned int m_alg=fmin(BLOCK_MR,m-ic);
        skipPos =BLOCK_MR - m_alg;
		for(unsigned int pc=0;pc<k;pc++){

			/*for(unsigned int ir=0;ir<m_alg;ir++){
                    A_pack_local[0]=A[(ic+ir)+pc*lda];
                    A_pack_local++;
                    
            }*/
            stransPrecExpand(type_desc, m_alg,&A[ic + pc*lda],A_pack_local);
            
            A_pack_local += (skipPos + m_alg);
		}

	}
}


/** Packing of simple precision matrix B.
 * 
 * Packs a block of matrix B into a buffer B_pack in the proper data arrengment 
 *  that the microkernel needs.
 * 
 * @param[in] B Pointer pointing to the position of B to start packing.
 * @param[in] ldb Leading dimension of matrix B.
 * @param[out] B_pack Buffer containing the portion of B packed.
 * @param[in] n Width of the block to pack.
 * @param[in] k Height of the block to pack.
 */
void sPack_B(float *B, unsigned int ldb, float *B_pack, unsigned int k, unsigned int n)
{
	float *B_pack_local;
    unsigned int skipPos;

	#pragma omp parallel for private(B_pack_local,skipPos)
	for(unsigned int jc=0;jc<n;jc+=BLOCK_NR){

		B_pack_local=&B_pack[jc*k];
		unsigned int n_alg=fmin(BLOCK_NR,n-jc);
        skipPos =BLOCK_NR - n_alg;
		for(unsigned int pc=0;pc<k;pc++){

			for(unsigned int jr=0;jr<n_alg;jr++){
				B_pack_local[0]=B[pc+jc*ldb+jr*ldb];
				B_pack_local++;
			}
            B_pack_local+=skipPos;
		}

	}
}

/** Single precision matrix matrix multiplication.
 * 
 * Performs a matrix matrix product in the form C = alpha * AB + beta * C. Expects matrices stored in column major order.
 * 
 * @param[in] m Number of rows of matrix C and A.
 * @param[in] n Number of columns of matrix C and B.
 * @param[in] k Number of columns of matrix A and rows of matrix B.
 * @param[in] alpha Scalar alpha .
 * @param[in] A Matrix A.
 * @param[in] lda Leading dimension of matrix A.
 * @param[in] B Matrix B.
 * @param[in] ldB Leading dimension of matrix B.
 * @param[in] beta Scalar beta. 
 * @param[in,out] C Matrix C.
 * @param[in] ldc Leading dimension of matrix C.
 * @param[in] Ac_pack_v Workspace for the packing of A (Only ofr allocation purposes).
 * @param[in] Bc_pack_v Workspace for the packing of B (Only ofr allocation purposes).
 */
void sgemm_transPrec(unsigned int m, unsigned int n, unsigned int k,
		float alpha, fpType type_desc,
		bits * A, unsigned int lda,
		float * B, unsigned int ldb,
		float beta,
		float * C, unsigned int ldc,
        void * Ac_pack_v, void * Bc_pack_v){
            
	bits *Ac;
    float *Bc;
	float *Cc;
	float *Ar, *Br;
	float *Cr;
	float betaInner, zero =0.0;
    
    
    
    
    float *Ac_pack=(float *)Ac_pack_v;
	float *Bc_pack=(float *)Bc_pack_v;
    float CBuff[BLOCK_MR*BLOCK_NR];
    bli_sset0s_mxn(BLOCK_MR,BLOCK_NR,CBuff,1,BLOCK_MR);
    
	for (unsigned int jc=0; jc<n; jc+=BLOCK_NC) {

		unsigned int n_alg=fmin(BLOCK_NC,n-jc);
		for (unsigned int pc=0; pc<k; pc+=BLOCK_KC) {

			unsigned int k_alg=fmin(BLOCK_KC,k-pc);
			if (pc >= BLOCK_KC) //Check beta
				betaInner=1.0;
			else
				betaInner=beta;

			Bc=&B[pc+jc*ldb];
			sPack_B(Bc, ldb, Bc_pack, k_alg, n_alg);  //PACK B

			
			for (unsigned int ic=0; ic<m; ic+=BLOCK_MC) {

				unsigned int m_alg=fmin(BLOCK_MC,m-ic);
				float *Ac_pack_local=Ac_pack; 

				Ac= (bits*)&A[ic+pc*lda];
				sPack_A_transPrec(type_desc, Ac,lda,(float*)Ac_pack_local,m_alg,k_alg); //PACK A

				Cc=&C[ic+jc*ldc];

                

				#pragma omp  parallel for private(Ar, Br, Cr,CBuff)
				for(unsigned jr=0;jr<n_alg;jr+=BLOCK_NR){
					unsigned int nr_alg=fmin(BLOCK_NR,n_alg-jr);
					for(unsigned int ir=0;ir<m_alg;ir+=BLOCK_MR){
						unsigned int mr_alg=fmin(BLOCK_MR,m_alg-ir);
						Ar=&Ac_pack_local[ir*k_alg];
						Br=&Bc_pack[jr*k_alg];
						Cr=&Cc[ir+jr*ldc];

						if(mr_alg==BLOCK_MR && nr_alg==BLOCK_NR)
						{
                            sgemm_armv8a_asm_8x12(k_alg,&alpha,Ar,Br,&betaInner,Cr,1,ldc);
						}
						else{//Micro-kernel cannot be applied
                            sgemm_armv8a_asm_8x12(k_alg,&alpha,Ar,Br,&zero,CBuff,1,BLOCK_MR);
                            sxpbyM(mr_alg, nr_alg, CBuff, BLOCK_MR, &betaInner, Cr, ldc);
                            //bli_sssxpbys_mxn(mr_alg,nr_alg,CBuff,1,BLOCK_MR,&betaInner,Cr,1,ldc);
						}
					}
				}

			}
		}
	}
}



/** Packing of B + im2Col transform
 * 
 * Packs matrix B = im2Col(In) into the buffer B_pack in the proper data arrengment 
 *  that the microkernel needs. Matrix B does not exist in memory and the data packed 
 *  into B_pack is read from the corresponding positions of the input tensor (In), 
 *  resulting in an on-the-fly im2col transform. 
 * 
 * @param[in] i Row index in matrix B of the first position of the block to pack. 
 * @param[in] j Column index in matrix B of the first position of the block to pack .
 * @param[in] In Input tensor.
 * @param[out] B_pack Buffer containing the portion of B packed.
 * @param[in] k Height of the block to pack.
 * @param[in] n Width of the block to pack.
 * @param[in] b Batch size.
 * @param[in] c number of chanels of input tensor.
 * @param[in] h input tensor hight.
 * @param[in] w input tensor width.
 * @param[in] ho matrix B hight.
 * @param[in] wo imatrix B width.
 * @param[in] kh kernel height.
 * @param[in] kw kernel width.
 * @param[in] stride Stride to apply the krnels to the input tensor.
 */
void sPack_im2Col(unsigned int i, unsigned int j,float * restrict In, float * restrict B_pack, unsigned int k, unsigned int n, unsigned int b, unsigned int c, 
                 unsigned int h, unsigned int w, 
                 unsigned int ho, unsigned int wo,
                 unsigned int kh, unsigned int kw, unsigned int stride)

{
    unsigned int ic,ikw,ikh, //Row related indexes (regarding the phantom matrix)
                 j_local, ib,iw,ih, //Col related indexes (regarding the phantom matrix)
                 pos, pos_ic, pos_ib, pos_ic_ikw; //position on the original image
    unsigned int pos_ic_ini,ikw_ini,ikh_ini,pos_ib_ini,iw_ini,ih_ini; //Initial values of indexes
    
    unsigned int cSize = h*w, //chanel memory leap in input tensor
                 coSize = ho*wo, //chanel memory leap in matrix B
                 kSize = kh*kw, //kernel memory leap (single chanel)
                 bSize = c*h*w; //batch memory leap
    
    unsigned int jc,pc,jr; //loop control indexes
	float * restrict B_pack_local;
    unsigned int skipPos;
    
    ic = i/kSize;
    ikw_ini = (i%kSize)/kh;
    ikh_ini = (i%kSize)%kh;
    pos_ic_ini = ic * cSize;



    #pragma omp parallel for private(B_pack_local,skipPos, j_local,pc,jr,ib,ih_ini, iw_ini, pos_ib_ini,pos_ic,ikw,pos_ic_ikw,ikh,pos_ib,iw,ih,pos) firstprivate(j)
	for(jc=0;jc<n;jc+=BLOCK_NR){

		B_pack_local=&B_pack[jc*k];
		unsigned int n_alg=fmin(BLOCK_NR,n-jc);
        skipPos =BLOCK_NR - n_alg;
         
        j_local = j +jc;
        ib = j_local/coSize;
        iw_ini = (j_local%(coSize))/ho;
        ih_ini = (j_local%(coSize))%ho;
        pos_ib_ini = ib * bSize;

        

        //ih_ini = ih_ini + jc
        
        pos_ic=pos_ic_ini;
        ikw=ikw_ini;
        pos_ic_ikw = ikw * h + pos_ic;
		for(pc=0,ikh=ikh_ini;pc<k;pc++,ikh++){
            if(ikh==kh)
            {
                ikh=0;
                ikw++;
                pos_ic_ikw += h; //OPT pos_ic_ikw = ikw* h +pos_ic
                if(ikw==kw)
                {
                    ikw=0;
                    pos_ic += cSize;//OPT ic++;pos_ic = ic * cSize;
                    pos_ic_ikw = pos_ic;//OPT pos_ic_ikw = ikw *h + pos_ic;
                }
            }
            
            pos_ib=pos_ib_ini;
            iw=iw_ini;
			for(jr=0,ih=ih_ini;jr<n_alg;jr++,ih++){
                if(ih==ho)
                {
                    ih=0;
                    iw++;
                    if(iw==wo)
                    {
                        iw=0;
                        pos_ib += bSize;//OPT ib++;pos_in = ib*bSize;
                    }
                }
                //OPT pos = ib * bSize  + ic * cSize + (iw*stride + ikw) *h + (ih * stride+ikh);
                //OPT pos = pos_ib + pos_ic + (iw*stride*h + pos_ikw) + (ih * stride+ikh);
				pos = pos_ib + pos_ic_ikw + iw*stride*h + (ih * stride+ikh);
                
                B_pack_local[0]=In[pos];
				B_pack_local++;
			}
			B_pack_local+=skipPos;
		}
        //ih_ini = ih;
        //iw_ini = iw;
        //pos_ib_ini = pos_ib;
	}
}


/** Simple precision matrix matrix multiplication with implicit im2col.
 * 
 * Performs a matrix matrix product in the form C = alpha * AB + beta * C, where B = im2col(In). Expects matrices stored in column major order.
 * 
 * @param[in] kh Kernel height.
 * @param[in] kw Kernel width.
 * @param[in] c Number of chanels of input tensor.
 * @param[in] kn Kernel number.
 * @param[in] alpha Scalar alpha.
 * @param[in] A Matrix A. lda assumed as kn.
 * @param[in] h Input tensor hight.
 * @param[in] w Input tensor width.
 * @param[in] b Batch size.
 * @param[in] stride Stride to apply the krnels to the input tensor.
 * @param[in] In 1D-array containing a flattened version of the input tensor.
 * @param[in] beta Scalar beta. 
 * @param[in,out] C Matrix C. ldc asumed as kn.
 * @param[in] Ac_pack Workspace for the packing of A (Only ofr allocation purposes).
 * @param[in] Bc_pack Workspace for the packing of B (Only ofr allocation purposes).
 */
void sconvGemm_transPrec(unsigned int kh, unsigned int kw, unsigned int c, unsigned int kn,
		float alpha, 
        fpType type_desc, bits * A, 
        unsigned int h, unsigned int w, unsigned int b, unsigned int stride,
		float * In, float beta,
		float * C,
        float * Ac_pack, float * Bc_pack ){
            
	bits *Ac;
    float *Bc;
	float *Cc;
	float *Ar, *Br;
	float *Cr;
	float betaInner, zero =0.0;
    
    unsigned int ho, wo, pad = 0;//padding currently unsuported
    
    ho = floor((h - kh + 2 * pad) / stride + 1);
    wo = floor((w - kw + 2 * pad) / stride + 1);

    unsigned int m = kn,
                 n = ho*wo*b,
                 k = kh*kw*c;
          
    unsigned int lda= kn,
                 ldc= kn;
                 
    
    float CBuff[BLOCK_MR*BLOCK_NR];
    sset0sM(BLOCK_MR, BLOCK_NR, CBuff, BLOCK_MR);
    
	for (unsigned int jc=0; jc<n; jc+=BLOCK_NC) {

		unsigned int n_alg=fmin(BLOCK_NC,n-jc);
		for (unsigned int pc=0; pc<k; pc+=BLOCK_KC) {

			unsigned int k_alg=fmin(BLOCK_KC,k-pc);
			if (pc >= BLOCK_KC) //Check beta
				betaInner=1.0;
			else
				betaInner=beta;

			sPack_im2Col(pc,jc, In, Bc_pack, k_alg, n_alg, b,c,h,w,ho,wo,kh,kw, stride);  //PACK B

			for (unsigned int ic=0; ic<m; ic+=BLOCK_MC) {

				unsigned int m_alg=fmin(BLOCK_MC,m-ic);
				float *Ac_pack_local=Ac_pack; 

				Ac=(bits*)&A[ic+pc*lda];
				sPack_A_transPrec( type_desc, Ac,lda,(float*)Ac_pack_local,m_alg,k_alg); //PACK A

				Cc=&C[ic+jc*ldc];


                #pragma omp parallel for  private(Ar, Br, Cr,CBuff) 
				for(unsigned jr=0;jr<n_alg;jr+=BLOCK_NR){
					unsigned int nr_alg=fmin(BLOCK_NR,n_alg-jr);
					for(unsigned int ir=0;ir<m_alg;ir+=BLOCK_MR){
						unsigned int mr_alg=fmin(BLOCK_MR,m_alg-ir);
						Ar=&Ac_pack_local[ir*k_alg];
						Br=&Bc_pack[jr*k_alg];
						Cr=&Cc[ir+jr*ldc];

						if(mr_alg==BLOCK_MR && nr_alg==BLOCK_NR)
                            sgemm_armv8a_asm_8x12(k_alg,&alpha,Ar,Br,&betaInner,Cr,1,ldc);
						else{//Micro-kernel cannot be applied
                            sgemm_armv8a_asm_8x12(k_alg,&alpha,Ar,Br,&zero,CBuff,1,BLOCK_MR);
                           // bli_sssxpbys_mxn(mr_alg,nr_alg,CBuff,1,BLOCK_MR,&betaInner,Cr,1,ldc);
                            sxpbyM(mr_alg, nr_alg, CBuff, BLOCK_MR, &betaInner, Cr, ldc);
                        }
					}
				}

			}
		}
	}
}


int stransPrecExpand(fpType type, unsigned n, void* fp_nums, void* s_nums)
{
    unsigned i;
    bits fp, fp_s, fp_e, fp_m;
    
    bits* fpV = (bits*)  fp_nums;
    bits32* sV = (bits32*)  s_nums;

    
    
    uint32_t  m_diff = fp32.bits_m - type.bits_m; //mantissa bit difference
    int32_t bias_corr = fp32.bias_e - type.bias_e, //Exponent bias correction
            exp_corr;  
    
    
    for(i=0; i < n; i++)
    {
        fp =  fpV[i];
        
        if( (fp & type.mask_zero) == 0 ) // fp is 0
            sV[i] = ((bits32) fp) << type.bits; //shift to return a 0 with the proper sign
        else
        {
            fp_s = fp & type.mask_s; //extract sign
            fp_e = fp & type.mask_e; // extract exponent
            fp_m = fp & type.mask_m; //extract mantissa
            
            if(fp_e == 0) //Normalize subnormal values
            {
                exp_corr = -1;
                do
                {
                    exp_corr++;
                    fp_m <<= 1;
                }
                while((fp_m & type.mask_hidden) == 0);
                
                sV[i] = ((bits32) fp_s) << type.bits | //sign
                        ((bits32) ((( (int32_t) (fp_e >> type.bits_m)) + bias_corr - exp_corr)  << fp32.bits_m)) |//exponent with corrected bias
                        ((bits32) (fp & type.mask_m )) <<  m_diff; //mantisa
            }
            else if(fp_e == type.mask_e) // all exponent bits 1 (Inf or NaN)
            {
                // Sign + exponent(1s) + lower type mantissa shifted to msb
                sV[i] = (((bits32) fp) << type.bits) | fp32.mask_e | ( ((bits32) fp_m) << m_diff); 
            }
            else //normalized number
            {
                sV[i] = ((bits32) fp_s) << type.bits | //sign
                        (((fp_e >> type.bits_m) + bias_corr)  << fp32.bits_m) |//exponent with corrected bias
                        ((bits32) fp_m) <<  m_diff; //mantisa
            }
        }
    }

    
    return 0;
}

int stransPrecShrink(fpType type, unsigned n, void* s_nums, void* fp_nums)
{
    
    unsigned i;
    bits32 s, s_s, s_e, s_m;
    bits fp_m;
    
    bits32* sV = (bits32*)  s_nums;
    bits* fpV = (bits*)  fp_nums;
    
    
    uint32_t  m_diff = fp32.bits_m - type.bits_m; //mantissa bit difference
    int32_t bias_corr = type.bias_e - fp32.bias_e; //Exponent bias correction 
    int32_t fp_exp;
    
    for(i=0; i < n; i++)
    {
        s =  sV[i];
        
        if( (s & fp32.mask_zero) == 0 ) // fp is 0
            fpV[i] = (bits) (s >> type.bits); //shift to return a 0 with the proper sign
        else
        {
            s_s = s & fp32.mask_s; //extract sign
            s_e = s & fp32.mask_e; // extract exponent
            s_m = s & fp32.mask_m; //extract mantissa
            
            if(s_e == 0)//Subnormal numbers underflow during conversion
            {
                fpV[i] = (bits) (s_s >> type.bits); //shift to return a 0 with the proper sign
            }
            else if(s_e == fp32.mask_e) // all exponent bits 1 (Inf or NaN)
            {
                if(s_m == 0)
                    fpV[i]= (bits) ( (s_s >> type.bits) | type.mask_e);
                else
                    fpV[i] = (bits) ((s_s >> type.bits) | type.mask_nan);
            }
            else //normalized number
            {
                fp_exp = ((int32_t) (s_e >> fp32.bits_m )) + bias_corr;
                if(fp_exp > type.max_e) //Exponent overflow
                {
                    fpV[i] = (bits) ((s_s >> type.bits) | type.mask_e); //Infinite
                }
                else if(fp_exp <=0) //Exponent underflow, fp element will be subnormal
                {
                    s_m |= fp32.mask_hidden;
                    
                    if( (m_diff - fp_exp) > (fp32.bits_m)) // 14 -fp_exp > 24 Mantisa lost by overshifting, return signed 0
                    {
                        fpV[i]= (bits) (s_s >> type.bits);
                    }
                    else 
                    {
                        fp_m = (bits) (s_m >> (m_diff+1 - fp_exp));
                        fpV[i]= ((bits) (s_s >> type.bits) | //sign
                                fp_m) + //shifted mantisa
                                ((s_m >> (m_diff -fp_exp)) &  0x1u); //Round to nearest, tie rounds up
                    }
                }
                else 
                {
                    fpV[i]= (bits) ( (s_s >> type.bits)| //sign
                            ((bits) (fp_exp << type.bits_m)) | //exponent
                            s_m >> m_diff) + //mantisa
                            (s_m >> (m_diff-1) & 0x1u); //Round to nearest, tie rounds up
                }
            }
        }
    }

    
    return 0;
    
}
