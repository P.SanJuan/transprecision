/**GEMM and GEMM_conv codef
 * 
 * This file contains the declaration of all functions and constants related with the GEMM computation.
 * 
 * @author P. San Juan
 * @date 04/2020
 */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <string.h>
#include <omp.h>
#include <stdint.h>
#include <blis.h>

//Global variables used when we want to modify the block sizes at run time.
#ifdef runtimeBLCKS
    #define dBLOCK_NR 8
    #define dBLOCK_MR 6
    int dBLOCK_MC,
        dBLOCK_NC,
        dBLOCK_KC;
    #define hBLOCK_NR 8
    #define hBLOCK_MR 24
    int hBLOCK_MC,
        hBLOCK_NC,
        hBLOCK_KC;
    #define BLOCK_NR 12
    #define BLOCK_MR 8
    int BLOCK_MC,
        BLOCK_NC,
        BLOCK_KC;
#else
    //simple precission BLIS block sizes for NVIDIA Carmel//ARM A-57
    #define BLOCK_NC 3072
    #define BLOCK_KC 368//640
    #define BLOCK_MC 560//120
    #define BLOCK_NR 12
    #define BLOCK_MR 8
    #define MAX_THREAD 8

    //half precission BLIS block sizes for NVIDIA Carmel //ARM A-57
    #define hBLOCK_NC 3072
    #define hBLOCK_KC 672 //768
    #define hBLOCK_MC 576 //96
    #define hBLOCK_NR 8
    #define hBLOCK_MR 24
    #define hSUBB_NR 8
    #define hSUBB_MR 8
    #define hMAX_THREAD 8
    #define ALIGN 32
#endif

#define sExp 8
#define sSignif 23
#define masksExp 0x7F800000
#define masksSignif 0x7FFFFF
#define masksSign 0x80000000
 
/**/
        
//#ifdef fpLen_16
typedef struct fpType16{
    uint16_t bits;
    uint16_t bits_e;
    uint16_t bits_m;
    
    int16_t bias_e;
    int16_t max_e;
    
    uint16_t mask_s;
    uint16_t mask_e;
    uint16_t mask_m;
    uint16_t mask_zero;
    uint16_t mask_nan;
    uint16_t mask_hidden;
    
} fpType;

typedef uint16_t bits;
fpType fp16={16, 5, 10, 15, 30, 0x8000u, 0x7C00u, 0x03FFu, 0x7FFFu, 0x7E00u,0x0400u};
fpType bf16={16, 8, 7, 127, 254, 0x8000u, 0x7F80u, 0x007Fu, 0x7FFFu, 0x7FC0u,0x0080u};

//#endif
/*
typedef struct fpType24{
    uint24_t bits;
    uint24_t bits_e;
    uint24_t bits_m;
    
    int24_t bias_e;
    int24_t max_e;
    
    uint24_t mask_s;
    uint24_t mask_e;
    uint24_t mask_m;
    uint24_t mask_zero;
    uint24_t mask_nan;
    
} fpType24;
*/

typedef struct fpType32{
    uint32_t bits;
    uint32_t bits_e;
    uint32_t bits_m;
    
    int32_t bias_e;
    int32_t max_e;
    
    uint32_t mask_s;
    uint32_t mask_e;
    uint32_t mask_m;
    uint32_t mask_zero;
    uint32_t mask_hidden;
    
} fpType32;

typedef uint32_t bits32;
fpType32 fp32={32, 8, 23, 127,254, 0x80000000u, 0x7F800000u, 0x007FFFFFu, 0x7FFFFFFFu, 0x00800000u};



/*****transprecision converters *****/
int stransPrecExpand(fpType type, unsigned n, void* fpV, void* sV);
int stransPrecShrink(fpType type, unsigned n, void* sV, void* pfV);



/********Simple precision gemm********/
void sgemm_transPrec(unsigned int m, unsigned int n, unsigned int k,
		float  alphap, fpType type_desc,
		bits * A, unsigned int lda,
		float * B, unsigned int ldb,
		float  betap,
		float * C, unsigned int ldc,
        void * Ac_pack_v, void * Bc_pack_v);

//Microkernels
void sgemm_armv8a_asm_8x12(int k,float* restrict alpha, float* restrict a, float* restrict b, float* restrict beta, float* restrict c, int rs_c, int cs_c);

//Packing routines
void sPack_A_transPrec(fpType type_desc, bits *A, unsigned int lda, float *A_pack, unsigned int m, unsigned int k);
void sPack_B(float *B, unsigned int ldb, float *B_pack, unsigned int k, unsigned int n);

//Auxiliar function for buffer allcoation
int alloc_pack_buffs(float** Ac_pack, float** Bc_pack);

/********simple precision convolution gemm********/
void sconvGemm_transPrec(unsigned int kh, unsigned int kw, unsigned int c, unsigned int kn,
		float alpha, fpType type_desc, bits * A, 
        unsigned int h, unsigned int w, unsigned int b, unsigned int stride,
		float * B, float beta,
		float * C,
        float * Ac_pack, float * Bc_pack );

//Packing routine
void sPack_im2Col(unsigned int i, unsigned int j,float * restrict B, float * restrict B_pack, 
                  unsigned int k, unsigned int n, unsigned int b, unsigned int c, 
                  unsigned int h, unsigned int w, unsigned int ho, unsigned int wo, unsigned int kh, unsigned int kw, unsigned int stride);
            
